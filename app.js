const bill = document.getElementById('amount');   //enter bill
const tipBtns = document.querySelectorAll('.percentage'); //tip buttons
const persons = document.getElementById('people-form');  // enter people
const resetBtn = document.getElementById('reset');  //reset button
const tipPerson = document.querySelector('.tip-person');
const totalPerson = document.querySelector('.total-person');
const customTip = document.querySelector('.custom-input');
const errorMsg = document.querySelector('.error');

// console.log(errorMsg);

// console.log(tipBtns);
let billAmount = 0;
let tip = 0.05;
let people = 1; 


// setting billAmount
bill.addEventListener('keyup', billValue)
function billValue(){
    billAmount = parseFloat(bill.value);
}

//setting tip %
tipBtns.forEach((btn) =>{
    btn.addEventListener('click', percentage);
})
function percentage(e){
    tip = parseFloat(e.target.textContent);
    // calculate()
}

customTip.addEventListener('input', customPercentage);
function customPercentage(){
    tip = parseFloat(customTip.value);
    // calculate()
}



// setting number of persons
persons.addEventListener('input', numberOfPersons);
function numberOfPersons(){
    if(persons.value <= 0){
        // console.log(errorMsg);
        errorMsg.style.display = 'initial';
        persons.style.border = '2px solid red'
    }
    else{
    people = parseFloat(persons.value);
    errorMsg.style.display = 'none';
    persons.style.border = 'initial';

    }

    calculate();
}




function calculate(){
    let tipAmount = parseFloat((((billAmount*tip)/people)/100).toFixed(2));
    let totalPerPerson = ((billAmount/people)+tipAmount).toFixed(2);
    tipPerson.innerHTML ='$' + tipAmount
    totalPerson.innerHTML = '$' + totalPerPerson
}

resetBtn.addEventListener('click', reset)
function reset(){
    billAmount =0;
    tip=0;
    people = 0;
    tipPerson.innerHTML = 0;
    totalPerson.innerHTML = 0;
    bill.value = null;
    persons.value = null;
    customTip.value = null;

    // resetBtn.removeEventListener('click', reset);
}
